local composer = require("composer")
local scene = composer.newScene()
system.activate( "multitouch" )

--здесь размещать глобальные переменные и пользовательские функции
local physics, widget
local right = false
local left = false
local temp1 = true
local sc = 0

local function toL()
    if left then
        Fox.x = Fox.x - 1

        if ((Fox.x < 150) and (stopMap1.x < -42)) then
            preg1.x = preg1.x + 1
            preg2.x = preg2.x + 1
            preg3.x = preg3.x + 1
            preg4.x = preg4.x + 1
            preg5.x = preg5.x + 1
            preg6.x = preg6.x + 1
            preg7.x = preg7.x + 1

            if (eat ~= nill) then
                eat.x = eat.x + 1
            end

            stopMap1.x = stopMap1.x + 1
            stopMap2.x = stopMap2.x + 1

            if (grass1.x > 800) then
                grass1.x = -330
            else
                grass1.x = grass1.x + 1
            end
    
            if (grass2.x > 800) then
                grass2.x = -330
            else
                grass2.x = grass2.x + 1
            end
        end
    end
end

local function toR()
    if right then
        Fox.x = Fox.x + 1

        if (Fox.x > 180) then
            preg1.x = preg1.x - 1
            preg2.x = preg2.x - 1
            preg3.x = preg3.x - 1
            preg4.x = preg4.x - 1
            preg5.x = preg5.x - 1
            preg6.x = preg6.x - 1
            preg7.x = preg7.x - 1

            if (eat ~= nil) then
                eat.x = eat.x - 1
            end

            stopMap1.x = stopMap1.x - 1
            stopMap2.x = stopMap2.x - 1

            if (grass1.x < -330) then
                grass1.x = 800
            else
                grass1.x = grass1.x - 1
            end
    
            if (grass2.x < -330) then
                grass2.x = 800
            else
                grass2.x = grass2.x - 1
            end
        end
    end
end

local function toUp1()
    up:setEnabled(true)
    Fox:setSequence( "stay" )
    Fox:play()
end

local function toUp2()
    Fox:setLinearVelocity(0, -300)
    Fox:setSequence( "jump" )
    Fox:play()

    timerUp = timer.performWithDelay(1500, toUp1, 1)
    up:setEnabled(false)
end

local function scrollGrass(self, event)
    if (self.x < -350) then
        self.x = 800
    else
        grass1.x = grass1.x - 1
        grass2.x = grass2.x - 1
    end
end

local function EatMeat(self, event)
    if (event.phase == "began") then
        if (event.other.ID == "eat") then
            sc = sc + 1
            score.text = sc
            event.other:removeSelf()
            eat = nil
        end
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")
    physics = require("physics")
    physics.start()

    --фон
    fon = display.newRect(display.contentCenterX, display.contentCenterY, 570, 320)
    fon.fill = { type = "image", filename = "pictures/fon1.jpeg"}
    fon:toBack()

    --ПЕРСОНАЖ
    Fox.alpha = 1
    Fox:play()
    --координаты
    Fox.x = 110
    Fox.y = 280
    --размер
    Fox.width = 80
    Fox.height = 50
    --физика
    physics.addBody(Fox, "dynamic")
    Fox.isFixedRotation = true
    Fox.angularDamping = 10
    Fox.linearDamping = 2
    --прослушиватель
    Fox.collision = EatMeat
    Fox:addEventListener("collision", Fox)
    --анимация
    Fox:setSequence("stay")
    Fox:play()

    --счет
    Text1 = display.newText(  "Счет:", 450, 20, "font4", 20 )
    Text1:setFillColor( 0.2, 0.3, 0.6 )
    Text1:toFront()
    score = display.newText( sc, 500, 20, "font4", 20 )
    score:setFillColor( 0.2, 0.3, 0.6 )
    score:toFront()

    --стреляет персонаж
    function Shoot(event)
        if (event.phase == "began") then
            bul = display.newRect( Fox.x, Fox.y, 5, 30 )
            sceneGroup:insert(bul)
            bul.rotation = math.ceil(math.atan2((event.y - bul.y), (event.x - bul.x)) * 180 / math.pi) + 90
            bul:setFillColor( 0.8, 0.1, 0.1 )
            physics.addBody( bul, "dinamic", {isSensor = true} )
            bul.gravityScale = 0
            bul.ID = "bul"
            local ang = -math.rad(bul.rotation + 90)
            local vX = math.cos(ang)
            local vY = - math.sin(ang)
            bul:applyLinearImpulse( -0.07 * vX, -0.07 * vY, Fox.x, Fox.y)
            --shot()
        end
    end

    --ГРАНИЦЫ
    gran1 = display.newRect(display.contentCenterX, 315, 565, 10)
    gran1:setFillColor(1, 0, 0, 0)
    physics.addBody(gran1, "static")

    gran2 = display.newRect(60, display.contentCenterY, 3, 350)
    gran2:setFillColor(1, 0, 0, 0)
    physics.addBody(gran2, "static")

    gran3 = display.newRect(524, display.contentCenterY, 3, 350)
    gran3:setFillColor(1, 0, 0, 0)
    physics.addBody(gran3, "static")

    gran4 = display.newRect(display.contentCenterX, 0, 565, 3)
    gran4:setFillColor(1, 0, 0, 0)
    physics.addBody(gran4, "static")

    --начало движения карты
    kart1 = display.newRect(230, display.contentCenterY, 3, 350)
    kart1:setFillColor(1, 0, 1, 0)
    physics.addBody(kart1, "static")

    stopMap1 = display.newRect(-42, display.contentCenterY, 3, 350)
    stopMap1:setFillColor(1, 0, 0, 0)
    physics.addBody(stopMap1, "static")

    stopMap2 = display.newRect(2200, display.contentCenterY, 3, 350)
    stopMap2:setFillColor(1, 0, 0, 0)
    physics.addBody(stopMap2, "static")

    --трава
    grass1 = display.newRect(display.contentCenterX, 307, 585, 30)
    grass1.fill = { type = "image", filename = "pictures/grass.png" }

    grass2 = display.newRect(800, 307, 585, 30)
    grass2.fill = { type = "image", filename = "pictures/grass.png" }

    --преграды
    preg1 = display.newRect(450, 300, 135, 25)
    physics.addBody(preg1, "static")
    preg1:setFillColor(0.3, 0.3, 0.3)

    preg2 = display.newRect(650, 255, 135, 25)
    physics.addBody(preg2, "static")
    preg2:setFillColor(0.3, 0.3, 0.3)

    preg3 = display.newRect(1200, 300, 395, 25)
    physics.addBody(preg3, "static")
    preg3:setFillColor(0.3, 0.3, 0.3)

    preg4 = display.newRect(1270, 275, 255, 25)
    physics.addBody(preg4, "static")
    preg4:setFillColor(0.3, 0.3, 0.3)

    preg5 = display.newRect(1330, 250, 135, 25)
    physics.addBody(preg5, "static")
    preg5:setFillColor(0.3, 0.3, 0.3)

    preg6 = display.newRect(1520, 275, 100, 25)
    physics.addBody(preg6, "static")
    preg6:setFillColor(0.3, 0.3, 0.3)

    preg7 = display.newRect(1720, 270, 135, 90)
    physics.addBody(preg7, "static")
    preg7:setFillColor(0.3, 0.3, 0.3)

    --для баллов
    eat = display.newCircle( 450, 270, 15 )
    eat.ID = "eat"
    physics.addBody( eat, "dinamic" )
    eat.gravityScale = 0
    eat.isSensor = true

    --vert = {-290,-150, 290,-150, 290,170, -290,170}
    --forShoot = display.newPolygon(display.contentCenterX, display.contentCenterY, vert)
    --forShoot:setFillColor(0.5, 0.4, 0.5, 0.6)
    --forShoot:toBack()

    --пауза
    pause = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 30, height = 30, -- размеры кнопки
        --left = 260, top = -20, -- положение кнопки
        fontSize = 20, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "| |",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            Text = display.newText( "пауза", display.contentCenterX, 40, "font4", 25 )
            Text:setFillColor(1, 0.8, 0.2)
            sceneGroup:insert(Text)

            physics.pause()
            up:setEnabled(false)
            tapR:setEnabled(false)
            tapL:setEnabled(false)

            --огонек для звука
            s.alpha = 1
            s.x = 270
            s.y = 120

            --кнопка отключения звука
            sound.alpha = 1
            sound.x = display.contentCenterX
            sound.y = 120

            --кнопка вернуться в меню
            back = widget.newButton {
                shape = "roundedRect", -- форма кнопки
                raidus = 5, -- радиус скругления уголков
                width = 90, height = 40, -- размеры кнопки
                --left = 20, top = 450, -- положение кнопки
                fontSize = 25, -- размер шрифта
                fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
                labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
                label = "В меню",
                onPress = function(event)
                    audio.play( Click, {loops = 0} )
                    sound.x = -20
                    sound.y = 20
                    s.x = 10
                    s.y = 20
                    Fox.alpha = 0
                    composer.gotoScene("menu", {effect = "fade"})
                end
            }
            back.x = display.contentCenterX
            back.y = 200
            sceneGroup:insert(back)

            close = widget.newButton {
                shape = "roundedRect", -- форма кнопки
                  raidus = 5, -- радиус скругления уголков
                  width = 20, height = 20, -- размеры кнопки
                  fontSize = 18, -- размер шрифта
                  fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
                  labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
                  label = "x",
                  onPress = function(event)
                    audio.play( Click, {loops = 0} )
                    physics.start()
                    up:setEnabled(true)
                    tapR:setEnabled(true)
                    tapL:setEnabled(true)
                    Text.text = ''
                    Text = nil
                    s.alpha = 0
                    sound.alpha = 0
                    sound.x = -20
                    sound.y = 20
                    s.x = 10
                    s.y = 20
                    display.remove(back)
                    back = nil
                    display.remove(close)
                    close = nil
                end
            }
            close.x = 350
            close.y = 40
            sceneGroup:insert(close)
        end
    }
    pause.x = -20
    pause.y = 20

    --движение влево
    tapL = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 45, height = 45, -- размеры кнопки
        fontSize = 25, -- размер шрифта
        fillColor = { default = { 0.9, 0.4, 0.4 }, over = { 1, 0.5, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.8 }, over = { 1, 0.9, 0.9 } },
        label = "<",
        onEvent = function(event)
            if event.phase == "began" then
                left = true

                if temp1 then
                    Fox:scale(-1, 1)
                    temp1 = false
                end

                Fox:setSequence("run")
                Fox:play()
            elseif event.phase == "ended" then
                left = false

                Fox:setSequence("stay")
                Fox:play()
            end
        end
    }
    tapL.x = 380
    tapL.y = 290

    --движение вправо
    tapR = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 45, height = 45, -- размеры кнопки
        fontSize = 25, -- размер шрифта
        fillColor = { default = { 0.8, 0.8, 0.3 }, over = { 1, 1, 0.5 } },
        labelColor = { default = { 0.9, 1, 0.8 }, over = { 1, 1, 0.9 } },
        label = ">",
        onEvent = function(event)
            if event.phase == "began" then
                right = true

                if temp1 == false then
                    Fox:scale(-1, 1)
                    temp1 = true
                end

                Fox:setSequence("run")
                Fox:play()
            elseif event.phase == "ended" then
                right = false

                Fox:setSequence("stay")
                Fox:play()
            end
        end
    }
    tapR.x = 480
    tapR.y = 290

    --движение вверх
    up = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 45, height = 45, -- размеры кнопки
        fontSize = 25, -- размер шрифта
        fillColor = { default = { 0.5, 0.5, 0.8 }, over = { 0.6, 0.6, 0.9 } },
        labelColor = { default = { 0.6, 0.8, 0.8 }, over = { 0.8, 1, 1 } },
        label = "^",
        onPress = toUp2
    }
    up.x = -5
    up.y = 290

    sceneGroup:insert(preg1)
    sceneGroup:insert(preg2)
    sceneGroup:insert(preg3)
    sceneGroup:insert(preg4)
    sceneGroup:insert(preg5)
    sceneGroup:insert(preg6)
    sceneGroup:insert(preg7)
    sceneGroup:insert(gran1)
    sceneGroup:insert(gran2)
    sceneGroup:insert(gran3)
    sceneGroup:insert(gran4)
    sceneGroup:insert(kart1)
    sceneGroup:insert(stopMap1)
    sceneGroup:insert(stopMap2)
    sceneGroup:insert(grass1)
    sceneGroup:insert(grass2)
    sceneGroup:insert(eat)

    sceneGroup:insert(pause)
    sceneGroup:insert(tapR)
    sceneGroup:insert(tapL)
    sceneGroup:insert(up)
    sceneGroup:insert(Text1)
    sceneGroup:insert(score)
end

function scene:show(event)
    local phase = event.phase

    --forShoot:addEventListener("touch", Shoot)
    Runtime:addEventListener("enterFrame", toL)
    Runtime:addEventListener("enterFrame", toR)

    if (phase == "did") then
        composer.removeScene("levels")
    end
end

function scene:hide(event)
    local phase = event.phase

    if (phase == "will") then
        physics.stop()
        physics = nil
        widget = nil
        pause = nil
        tapR = nil
        tapL = nil
        up = nil
        gran1 = nil
        gran2 = nil
        gran3 = nil
        gran4 = nil
        stopMap1 = nil
        stopMap2 = nil
        kart1 = nil
        preg1 = nil
        preg2 = nil
        preg3 = nil
        preg4 = nil
        preg5 = nil
        preg6 = nil
        preg7 = nil
        eat = nil
        grass1 = nil
        grass2 = nil
        Text1 = nil
        score = nil
        Fox.alpha = 0
    end
    if (phase == "did") then
        composer.removeScene("one_level")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene