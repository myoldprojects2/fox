local composer = require("composer")
local scene = composer.newScene()

--здесь размещать глобальные переменные и пользовательские функции
local widget
local one, two, three, four, five, six, back, ques, list

function scene:create(event)
    local sceneGroup = self.view

    widget = require("widget")

    Fox.x = 460
    Fox:scale(-1, 1)

    --кнопка уровень 1
    one = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 20, top = 130, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "1",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            display.remove(fon)
            fon = nil
            sound.alpha = 0
            s.alpha = 0
            composer.gotoScene("one_level", {effect = "fade"})
        end
    }
    one.x = 110
    one.y = 100

    --кнопка уровень 2
    two = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 125, top = 130, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "2",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
        end
    }
    two.x = display.contentCenterX
    two.y = 100

    --кнопка уровень 3
    three = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 230, top = 130, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "3",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
        end
    }
    three.x = 370
    three.y = 100

    --кнопка уровень 4
    four = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 20, top = 270, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "4",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
        end
    }
    four.x = 110
    four.y = 220

    --кнопка уровень 5
    five = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 125, top = 270, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "5",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
        end
    }
    five.x = display.contentCenterX
    five.y = 220

    --кнопка уровень 6
    six = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 70, height = 75, -- размеры кнопки
        --left = 230, top = 270, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "6",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
        end
    }
    six.x = 370
    six.y = 220

    --кнопка вернуться в меню
    back = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 40, height = 40, -- размеры кнопки
        --left = 20, top = 450, -- положение кнопки
        fontSize = 25, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "<",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            composer.gotoScene("menu", {effect = "fade"})
        end
    }
    back.x = -18
    back.y = 295

    --кнопка вопрос
    ques = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 40, height = 40, -- размеры кнопки
        fontSize = 25, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "?",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            Text = display.newText( "выберите уровень", display.contentCenterX, 20, "font4", 23 )
            Text:setFillColor(0.6, 0.7, 0.2)
            sceneGroup:insert(Text)

            close = widget.newButton {
              shape = "roundedRect", -- форма кнопки
                raidus = 5, -- радиус скругления уголков
                width = 18, height = 18, -- размеры кнопки
                fontSize = 18, -- размер шрифта
                fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
                labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
                label = "x",
                onPress = function(event)
                    audio.play( Click, {loops = 0} )
                    Text.text = ''
                    Text = nil
                    display.remove(close)
                    close = nil
                end
            }
            close.x = 360
            close.y = 40
            sceneGroup:insert(close)
        end
    }
    ques.x = 480
    ques.y = 20

    sceneGroup:insert(one)
    sceneGroup:insert(two)
    sceneGroup:insert(three)
    sceneGroup:insert(four)
    sceneGroup:insert(five)
    sceneGroup:insert(six)
    sceneGroup:insert(back)
    sceneGroup:insert(ques)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("menu")--удалить предыдущую сцену
    end
end

function scene:hide(event)
    local phase = event.phase

    if (phase == "will") then
        widget = nil
        one = nil
        two = nil
        three = nil
        four = nil
        five = nil
        six = nil
        back = nil
        ques = nil
        Fox.alpha = 0
        Fox:scale(-1, 1)
    end
    if (phase == "did") then
        composer.removeScene("levels")
    end
end

function scene:destroy(event)
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene