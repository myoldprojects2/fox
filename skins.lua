local composer = require("composer")
local scene = composer.newScene()

-- прячем строку состояния
display.setStatusBar(display.HiddenStatusBar);

local widget
local i = 1
skin.alpha = 1
local skins = {"colors/white.jpeg", "colors/red.jpg", "colors/blue.jpg", "colors/green.jpeg", "colors/purple.jpg"}

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")

    r = widget.newButton {
        shape = 'roundedRect', -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 35, height = 50, -- размеры кнопки
        left = 235, top = 210, -- положение кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = ">",
        onPress = function(event)
            if (i < 5) then
                i = i + 1
                skin.fill = { type = "image", filename = skins[i] }
                Start_click = audio.play( Click, {loops = 0} )
            end
        end
    }
    r.x = 360
    r.y = display.contentCenterY

    l = widget.newButton {
        shape = 'roundedRect', -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 35, height = 50, -- размеры кнопки
        fontSize = 30, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "<",
        onPress = function(event)
            if (i > 1) then
                i = i - 1
                skin.fill = { type = "image", filename = skins[i] }
                Start_click = audio.play( Click, {loops = 0} )
            end
        end
    }
    l.x = 120
    l.y = display.contentCenterY

    --кнопка применить
    ok = widget.newButton {
        shape = "roundedRect", -- форма кнопкиs
	    raidus = 5, -- радиус скругления уголков
        width = 120, height = 30, -- размеры кнопки
        fontSize = 20, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "применить",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            skin.alpha = 0
            composer.gotoScene("menu", {effect = "fade"})
        end
    }
    ok.x = display.contentCenterX
    ok.y = 250

    sceneGroup:insert(r)
    sceneGroup:insert(l)
    sceneGroup:insert(ok)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        composer.removeScene("menu")
        composer.removeScene("levels")
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        --physics.stop()
        widget = nil
        --display.remove(skin)
        r = nil
        l = nil
        ok = nil
    end
    if (phase == "did") then
        composer.removeScene("skins")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene