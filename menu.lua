local composer = require("composer")
local scene = composer.newScene()

local widget
local note
local levels
s.alpha = 1
sound.alpha = 1

function scene:create(event)--графика и аудио
    local sceneGroup = self.view

    widget = require("widget")

    fon = display.newRect(display.contentCenterX, display.contentCenterY, 570, 320)
    fon.fill = { type = "image", filename = "pictures/fon.png"}
    fon:toBack()

    Fox.alpha = 1
    Fox.x = 50
    Fox.y = 250
    Fox:setSequence("start")
    Fox:play()

    --кнопка сведений об игре
    note = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 180, height = 35, -- размеры кнопки
        fontSize = 18, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        font = "font4",
        label = "Сведения об игре",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            --composer.gotoScene("Spravka", {effect = "fade"})
        end
    }
    note.x = display.contentCenterX
    note.y = 260

    --кнопка перехода к уровням
    levels = widget.newButton {
        shape = "roundedRect", -- форма кнопки
	    raidus = 5, -- радиус скругления уголков
        width = 120, height = 120, -- размеры кнопки
        fontSize = 40, -- размер шрифта
        fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        label = "|>",
        onPress = function(event)
            audio.play( Click, {loops = 0} )
            composer.gotoScene("levels", {effect = "fade"})
        end
    }
    levels.x = display.contentCenterX
    levels.y = display.contentCenterY

    --кнопка персонаж
    --man = widget.newButton {
        --shape = "roundedRect", -- форма кнопки
	    --raidus = 5, -- радиус скругления уголков
        --width = 50, height = 50, -- размеры кнопки
        --left = -40, top = 50, -- положение кнопки
        --fontSize = 15, -- размер шрифта
        --fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
        --labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
        --label = "o\n/|\\\n/\\",
        --onPress = function(event)
            --audio.play( Click, {loops = 0} )
            --display.remove(man)
            --man = nil
            --composer.gotoScene("skins", {effect = "fade"})
        --end
    --}

    sceneGroup:insert(note)
    sceneGroup:insert(levels)
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        --composer.removeScene("Spravka")
        composer.removeScene("levels")
        composer.removeScene("skins")
        composer.removeScene("one_level")
        --composer.removeScene("two_level")
        --composer.removeScene("three_level")
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        widget = nil
        note = nil
        levels = nil
    end
    if (phase == "did") then
        composer.removeScene("menu")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene