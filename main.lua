local composer = require("composer")
widget = require("widget")

Click = audio.loadSound( "audio/клик.mp3" )

--установка скина по умолчанию
runFoxOptions = {
    frames = {
        --1
        {
            x = 0,
            y = 10,
            width = 145,
            height = 45
        },

        --2
        {
            x = 165,
            y = 2,
            width = 135,
            height = 55
        },

        --3
        {
            x = 315,
            y = -3,
            width = 135,
            height = 60
        },

        --4
        {
            x = 460,
            y = -3,
            width = 145,
            height = 60
        }
    }
}

runFox = graphics.newImageSheet( "pictures/Fox_1.png", runFoxOptions )

stayFoxOptions = {
    frames = {
        --1
        {
            x = 0,
            y = 0,
            width = 90,
            height = 105
        },

        --2
        {
            x = 100,
            y = 0,
            width = 113,
            height = 105
        },

        --3
        {
            x = 200,
            y = 30,
            width = 120,
            height = 70
        }
    }
}

stayFox = graphics.newImageSheet( "pictures/Fox_2_2.png", stayFoxOptions )

startFoxOptions = {
    frames = {
        --1
        {
            x = 0,
            y = 0,
            width = 110,
            height = 70
        },

        --2
        {
            x = 114,
            y = 0,
            width = 118,
            height = 70
        }
    }
}

startFox = graphics.newImageSheet( "pictures/Fox_3.png", startFoxOptions )

--бег
moveFox = {
    {
        name = "stay",
        sheet = stayFox,
        frames = {1, 2},
        time = 800
    },
    
    {
        name = "run",
        sheet = runFox,
        frames = {4, 1, 2, 3},
        time = 500
    },

    {
        name = "jump",
        sheet = stayFox,
        frames = {3},
        time = 500,
        loopCount = 1
    },

    {
        name = "start",
        sheet = startFox,
        frames = {1, 2},
        time = 800
    },
}

Fox = display.newSprite( stayFox, moveFox )
Fox.alpha = 0

--огонек для звука
s = display.newCircle(10, 20, 6)
s:setFillColor(0, 1, 0)
s.alpha = 0

temp = true

--кнопка отключения звука
sound = widget.newButton {
    shape = "roundedRect", -- форма кнопки
    raidus = 5, -- радиус скругления уголков
    width = 40, height = 40, -- размеры кнопки
    left = -40, top = 0, -- положение кнопки
    fontSize = 18, -- размер шрифта
    fillColor = { default = { 0.3, 0.3, 0.4 }, over = { 0.4, 0.4, 0.5 } },
    labelColor = { default = { 1, 0.8, 0.2 }, over = { 1, 0.9, 0.7 } },
    label = "<))",
    onPress = function(event)
        if (temp == true) then
            audio.setVolume( 0 )
            s:setFillColor(1, 0, 0)
            temp = false
        elseif (temp == false) then
            audio.play( Click, {loops = 0} )
            audio.setVolume( 1 )
            s:setFillColor(0, 1, 0)
            temp = true
        end
    end
}
sound.x = -20
sound.y = 20
sound.alpha = 0

composer.gotoScene("start_game")