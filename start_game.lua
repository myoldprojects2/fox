local composer = require("composer")
local scene = composer.newScene()

--фон
local icon
local timeLimit = 5
local timer1

--давным давно
local function begin()
    icon = display.newImage("icon.png", display.contentCenterX, display.contentCenterY)
end

local function timerDown()
    timeLimit = timeLimit - 1

    if(timeLimit == 4)then
        begin()
    end

    if (timeLimit == 1) then
        display.remove(icon)
    end

    if(timeLimit == 0)then
        composer.gotoScene("menu", {effect = "flip"})
    end
end

function scene:create(event)--графика и аудио
    local sceneGroup = self.view
end

function scene:show(event)--визуализация показа сцены
    local phase = event.phase

    if (phase == "did") then
        timer1 = timer.performWithDelay(1000, timerDown, timeLimit)
    end
end

function scene:hide(event)--вызывается когда закрывается сцена
    local phase = event.phase

    if (phase == "will") then--тут можно отключить флновую музыку и тд
        icon = nil
        timeLimit = nil
        timer1 = nil
    end
    if (phase == "did") then
        composer.removeScene("start_game")
    end
end

function scene:destroy(event)--для освобождения памяти от сцены
    -- body
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene